# Store Point of Sale

Desktop Point of Sale app built with electron && with web acces Fork from https://github.com/tngoman/Store-POS

!!!No realy secured, just a POC!!!

**Features:**

- Can be used by multiple PC's on a network with one central database.
- Receipt Printing.
- Search for product by barcode.
- Staff accounts and permissions.
- Products and Categories.
- Basic Stock Management.
- Open Tabs (Orders).
- Customer Database.
- Transaction History.
- Filter Transactions by Till, Cashier or Status.
- Filter Transactions by Date Range.

**To Customize/Create your own installer**

- Clone this project.
- Open terminal and navigate into the cloned folder.
- Run "npm install" to install dependencies.
- Run "npm run electron".

![POS](https://github.com/tngoman/Store-POS/blob/master/screenshots/pos.jpg)

![Transactions](https://github.com/tngoman/Store-POS/blob/master/screenshots/transactions.jpg)

![Receipt](https://github.com/tngoman/Store-POS/blob/master/screenshots/receipt.jpg)

![Permissions](https://github.com/tngoman/Store-POS/blob/master/screenshots/permissions.jpg)

![Users](https://github.com/tngoman/Store-POS/blob/master/screenshots/users.jpg)
