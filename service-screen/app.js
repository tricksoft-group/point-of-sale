/**
 * @author Nassim Ourami
 */

let endpoint = 'http://localhost:8001/api';
let localDatabase = {};
let $orderTemplate = document.getElementById('order-template');
let $content = document.getElementById('order-list');
let start = new Date();
start.setHours(0);
start.setMinutes(0);
start.setSeconds(1);

let end = new Date();
end.setHours(23);
end.setMinutes(59);
end.setSeconds(59);
end.setDate(end.getDate() + 1);

let settings = {
	symbol: '€',
};
/**
 * Make an ajax call
 *
 * @param {string} method
 * @param {string} url
 * @param {object|string|FormData} datas
 * @param {function} xhr
 */
function _callAsync(method, url, datas = {}, xhr = null) {
	let body = datas;
	if (datas instanceof FormData) {
		body = [...datas];
	}

	let message = {
		method,
		cache: 'default',
		headers: {
			'Content-Type': 'application/json',
		},
	};

	if (method !== 'GET' && method !== 'HEAD') {
		//	message.body = new URLSearchParams(body);
		message.body = JSON.stringify(body);
	}

	return fetch(url, message)
		.then((response) => {
			if (response.ok) {
				return response.json();
			}
		})
		.then((response) => {
			if (typeof response === 'undefined') {
				return {};
			}

			if (typeof response.data !== 'undefined') {
			}

			return response;
		})
		.catch((error) => {});
}
function pool() {
	_callAsync(
		'GET',
		endpoint + '/by-date?till=0&user=0&status=1&kitchen_status=1&start=' + start + '&end=' + end,
	).then((orders) => {
		for (var i in orders) {
			if (orders[i].service_status || orders[i].kitchen_status != 1) {
				continue;
			}
			if (typeof localDatabase[orders[i]._id] != 'undefined') {
				continue;
			}

			createOrderElement(orders[i]);
		}
	});
}

function createOrderElement(order) {
	let $clone = document.importNode($orderTemplate.content, true);
	let $card = $clone.querySelectorAll('.card-body');
	$card[0].id = order._id;
	let $header = $clone.querySelectorAll('.card-header');
	$header[0].innerHTML = 'Commande de : ' + order.ref_number + '<br>';

	let $button = document.createElement('BUTTON');
	$button.classList.add('btn', 'btn-lg', 'btn-success', 'btn-block');
	$button.innerHTML = 'Envoyer';

	$button.addEventListener('click', () => {
		updateStatus(order);
	});

	$header[0].appendChild($button);

	let $footer = $clone.querySelectorAll('.card-footer');
	let interval = setInterval(() => {
		let hour = moment().subtract(moment(order.date));
		let time = parseInt(hour.format('mmss'));
		$footer[0].innerHTML = 'Commandé il y a : ' + hour.format('mm:ss');

		$header[0].classList.remove('alert-danger', 'alert-warning', 'alert-info');
		$footer[0].classList.remove('alert-danger', 'alert-warning', 'alert-info');

		if (time > 300) {
			$header[0].classList.add('alert-danger');
			$footer[0].classList.add('alert-danger');

			return;
		}
		if (time > 200) {
			$header[0].classList.add('alert-warning');
			$footer[0].classList.add('alert-warning');
			return;
		}
		if (time > 100) {
			$header[0].classList.add('alert-info');
			$footer[0].classList.add('alert-info');
			return;
		}
	}, 10000);
	$footer[0].innerHTML =
		'Commandé il y a : ' + moment().subtract(moment(order.date)).format('mm:ss');

	let contents = '<ul class="list-group list-group-flush">';
	for (var i in order.items) {
		contents +=
			'<li class="list-group-item">' +
			order.items[i].quantity +
			' x ' +
			order.items[i].product_name +
			'</li>';
	}
	contents += ' </ul>';
	$card[0].innerHTML = contents;

	$content.appendChild($clone);

	order.service_status = 0;
	localDatabase[order._id] = Object.assign({}, order, { $element: $card[0], interval: interval });
}

function updateStatus(order) {
	if (typeof localDatabase[order._id] != 'undefined') {
		let parent = localDatabase[order._id].$element.parentElement.parentElement.parentElement;

		localDatabase[order._id].$element.parentElement.classList.remove('animate__bounceInUp');
		localDatabase[order._id].$element.parentElement.classList.add(
			'animate__animated',
			'animate__bounceOutLeft',
		);
		setTimeout(() => {
			clearInterval(localDatabase[order._id].interval);
			parent.removeChild(localDatabase[order._id].$element.parentElement.parentElement);
		}, 1000);

		localDatabase[order._id].service_status = 1;
		order.service_status = 1;

		_callAsync('PUT', endpoint + '/new', order).catch((error) => {
			console.log(error);
		});
	}
}
setInterval(() => {
	pool();
}, 5000);
