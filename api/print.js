const app = require('express')();
const path = require('path');
const server = require('http').Server(app);
const bodyParser = require('body-parser');
const fs = require('fs'),
	exec = require('child_process').exec;
const pdf = require('html-pdf');
const escpos = require('escpos');

escpos.USB = require('escpos-usb');
const device = new escpos.USB();
const options = { encoding: 'utf8' };
const printer = new escpos.Printer(device, options);
let moment = require('moment');
const tux = path.join(__dirname, '../assets/images/logo.jpg');

// directory path
const dir = process.env.APPDATA + '/POS/ticket/';

// create new directory
fs.mkdir(dir, (err) => {
	if (err) {
	}
	console.log('Directory is created.');
});

app.get('/', function (req, res) {
	res.send('Print API');
});

app.post('/print', function (req, res) {
	var request = req.body;
	let file = process.env.APPDATA + '/POS/ticket/' + request.id + '.pdf';
	let order = request.data;
	pdf
		.create(request.content, {
			format: 'Letter',
		})
		.toFile(file, (err, res) => {
			if (err) {
				//res.send('pdf error: ' + error);
			}
			//exec('lp ' + file);
		});

	//	fs.writeFile(file, request.content, () => {});

	// get printer jobs
	//exec('lpq', function (error, stdout, stderr) {
	//	console.log('stdout: ' + stdout);
	//	console.log('stderr: ' + stderr);
	//	if (error !== null) {
	//		console.log('exec error: ' + error);
	//		res.send('exec error: ' + error);
	//	}
	//	res.send('stdout: ' + stdout + ' ||| stderr: ' + stderr);
	//});

	if (order.paid > 0 && order.ticket == 1) {
		escpos.Image.load(tux, function (logo) {
			device.open(function (error) {
				printer.size(0, 0);
				printer.encode('utf8');
				printer
					.font('a')
					.encode('utf8')
					.align('CT')
					.style('NORMAL')
					.text('Festival de Gavarnie')
					.text("Place de l'Eglise")
					.text('65120 Gavarnie')
					.text('SIRET: 88535834100018')
					.text('------------------------------')
					.table(['Date', moment(order.date).format('DD/MM/YYYY hh:mm')])
					.table(['Num', order.order])
					.align('LT')
					.text('------------------------------')
					.table(['Qte', 'Produit', 'Prix']);

				order.items.forEach((item) => {
					printer.tableCustom([
						{ text: item.quantity + ' x ', align: 'LEFT', width: 0.1 },
						{ text: item.product_name, align: 'LEFT', width: 0.3 },
						{ text: parseFloat(item.price).toFixed(2) + ' EUR', align: 'LEFT', width: 0.6 },
					]);
				});

				printer
					.text('------------------------------')
					.table(['Total', order.subtotal + ' EUR'])
					.table(['dont consignes', order.deposit + ' EUR' ?? 0 + ' EUR'])
					.table(['dont TVA', order.tax + ' EUR'])
					.text('------------------------------')
					.table(['Reglement ', order.paid + ' EUR'])
					.table(['Rendu ', order.change + ' EUR'])
					.align('CT')

					.text('------------------------------')
					.text('Merci pour votre visite!')
					.image(logo, 's8')
					.then(() => {
						printer.cut().close();
					});
			});
		});
	}
});

module.exports = app;
