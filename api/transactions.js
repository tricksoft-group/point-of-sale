let app = require('express')();
let server = require('http').Server(app);
let bodyParser = require('body-parser');
let Datastore = require('nedb');
let Inventory = require('./inventory');
let moment = require('moment');

const ObjectsToCsv = require('node-create-csv');

app.use(bodyParser.json());

module.exports = app;

let transactionsDB = new Datastore({
	filename: process.env.APPDATA + '/POS/server/databases/transactions.db',
	autoload: true,
});

transactionsDB.ensureIndex({ fieldName: '_id', unique: true });

app.get('/', function (req, res) {
	res.send('Transactions API');
});

app.get('/all', function (req, res) {
	transactionsDB.find({}, function (err, docs) {
		res.send(docs);
	});
});

app.get('/json', function (req, res) {
	// Sample data - two columns, three rows:

	new Promise((resolve) => {
		transactionsDB.find({}, function (err, docs) {
			let data = [];

			docs.forEach((transaction) => {
				let discount = transaction.discount != '' ? transaction.discount : 1;
				let date = moment(transaction.date);
				transaction.items.map((item) => {
					item.price = item.price * discount;
					item.date = date.format('DD/MM/YYYY');
				});
				data.push(...transaction.items);
			});
			resolve(data);
		});
	}).then((data) => {
		const csv = new ObjectsToCsv(data);

		// Save to file:
		csv.toDisk(process.env.APPDATA + '/POS/exports/transactions.csv', { showHeader: true });
		//res.send(data);
		res.download(process.env.APPDATA + '/POS/exports/transactions.csv');
		// If you use "await", code must be inside an asynchronous function:
	});
});

app.get('/on-hold', function (req, res) {
	transactionsDB.find({ $and: [{ ref_number: { $ne: '' } }, { status: 0 }] }, function (err, docs) {
		if (docs) res.send(docs);
	});
});

app.get('/customer-orders', function (req, res) {
	transactionsDB.find(
		{ $and: [{ customer: { $ne: '0' } }, { status: 0 }, { ref_number: '' }] },
		function (err, docs) {
			if (docs) res.send(docs);
		},
	);
});

app.get('/by-date', function (req, res) {
	let startDate = new Date(req.query.start);
	let endDate = new Date(req.query.end);

	if (req.query.user == 0 && req.query.till == 0) {
		transactionsDB.find(
			{
				$and: [
					{ date: { $gte: startDate.toJSON(), $lte: endDate.toJSON() } },
					{ status: parseInt(req.query.status) },
				],
			},
			function (err, docs) {
				if (docs) res.send(docs);
			},
		);
	}

	if (req.query.user != 0 && req.query.till == 0) {
		transactionsDB.find(
			{
				$and: [
					{ date: { $gte: startDate.toJSON(), $lte: endDate.toJSON() } },
					{ status: parseInt(req.query.status) },
					{ user_id: parseInt(req.query.user) },
				],
			},
			function (err, docs) {
				if (docs) res.send(docs);
			},
		);
	}

	if (req.query.user == 0 && req.query.till != 0) {
		transactionsDB.find(
			{
				$and: [
					{ date: { $gte: startDate.toJSON(), $lte: endDate.toJSON() } },
					{ status: parseInt(req.query.status) },
					{ till: parseInt(req.query.till) },
				],
			},
			function (err, docs) {
				if (docs) res.send(docs);
			},
		);
	}

	if (req.query.user != 0 && req.query.till != 0) {
		transactionsDB.find(
			{
				$and: [
					{ date: { $gte: startDate.toJSON(), $lte: endDate.toJSON() } },
					{ status: parseInt(req.query.status) },
					{ till: parseInt(req.query.till) },
					{ user_id: parseInt(req.query.user) },
				],
			},
			function (err, docs) {
				if (docs) res.send(docs);
			},
		);
	}
});

app.post('/new', function (req, res) {
	let newTransaction = req.body;
	transactionsDB.insert(newTransaction, function (err, transaction) {
		if (err) res.status(500).send(err);
		else {
			res.sendStatus(200);

			if (newTransaction.paid >= newTransaction.total) {
				Inventory.decrementInventory(newTransaction.items);
			}
		}
	});
});

app.put('/new', function (req, res) {
	let oderId = req.body._id;
	transactionsDB.update(
		{
			_id: oderId,
		},
		req.body,
		{},
		function (err, numReplaced, order) {
			console.log(err);
			if (err) res.status(500).send(err);
			else res.sendStatus(200);
		},
	);
});

app.post('/delete', function (req, res) {
	let transaction = req.body;
	transactionsDB.remove(
		{
			_id: transaction.orderId,
		},
		function (err, numRemoved) {
			if (err) res.status(500).send(err);
			else res.sendStatus(200);
		},
	);
});

app.get('/:transactionId', function (req, res) {
	transactionsDB.find({ _id: req.params.transactionId }, function (err, doc) {
		if (doc) res.send(doc[0]);
	});
});
