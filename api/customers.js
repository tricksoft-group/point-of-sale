const app = require('express')();
const server = require('http').Server(app);
const bodyParser = require('body-parser');
const Datastore = require('nedb');
const async = require('async');
const crypto = require('crypto');

const key = 'mapsoldie!vnUfj';

app.use(bodyParser.json());

module.exports = app;

let customerDB = new Datastore({
	filename: process.env.APPDATA + '/POS/server/databases/customers.db',
	autoload: true,

	//afterSerialization(plaintext) {
	//	const iv = crypto.randomBytes(16);
	//	const aes = crypto.createCipheriv('aes256', key, iv);
	//	let ciphertext = aes.update(plaintext);
	//	ciphertext = Buffer.concat([iv, ciphertext, aes.final()]);
	//	return ciphertext.toString('base64');
	//},
	//beforeDeserialization(ciphertext) {
	//	const ciphertextBytes = Buffer.from(ciphertext, 'base64');
	//	const iv = ciphertextBytes.slice(0, 16);
	//	const data = ciphertextBytes.slice(16);
	//	const aes = crypto.createDecipheriv('aes256', key, iv);
	//	let plaintextBytes = Buffer.from(aes.update(data));
	//	plaintextBytes = Buffer.concat([plaintextBytes, aes.final()]);
	//	return plaintextBytes.toString();
	//},
});

customerDB.ensureIndex({ fieldName: '_id', unique: true });

app.get('/', function (req, res) {
	res.send('Customer API');
});

app.get('/customer/:customerId', function (req, res) {
	if (!req.params.customerId) {
		res.status(500).send('ID field is required.');
	} else {
		customerDB.findOne(
			{
				_id: req.params.customerId,
			},
			function (err, customer) {
				res.send(customer);
			},
		);
	}
});

app.get('/all', function (req, res) {
	customerDB.find({}, function (err, docs) {
		res.send(docs);
	});
});

app.post('/customer', function (req, res) {
	var newCustomer = req.body;
	customerDB.insert(newCustomer, function (err, customer) {
		if (err) res.status(500).send(err);
		else res.sendStatus(200);
	});
});

app.delete('/customer/:customerId', function (req, res) {
	customerDB.remove(
		{
			_id: req.params.customerId,
		},
		function (err, numRemoved) {
			if (err) res.status(500).send(err);
			else res.sendStatus(200);
		},
	);
});

app.put('/customer', function (req, res) {
	let customerId = req.body._id;

	customerDB.update(
		{
			_id: customerId,
		},
		req.body,
		{},
		function (err, numReplaced, customer) {
			if (err) res.status(500).send(err);
			else res.sendStatus(200);
		},
	);
});
