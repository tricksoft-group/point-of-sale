let express = require('express');
let http = require('http');
const path = require('path');
let bodyParser = require('body-parser');
let api = express();
let serverApi = http.createServer(api);

const PORT = process.env.PORT || 8001;

console.log('Server started');
api.use(bodyParser.json());
api.use(bodyParser.urlencoded({ extended: false }));

api.all('/*', function (req, res, next) {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
	res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,X-Key');
	if (req.method == 'OPTIONS') {
		res.status(200).end();
	} else {
		next();
	}
});

api.get('/', function (req, res) {
	res.send('POS Server Online.');
});

api.use('/api/inventory', require('./api/inventory'));
api.use('/api/customers', require('./api/customers'));
api.use('/api/categories', require('./api/categories'));
api.use('/api/settings', require('./api/settings'));
api.use('/api/users', require('./api/users'));
api.use('/api/print', require('./api/print'));
api.use('/api', require('./api/transactions'));

serverApi.listen({ port: PORT, host: '0.0.0.0' }, () =>
	console.log(`Api Server Listening on PORT ${PORT}`),
);

let app = express();
let serverApp = http.createServer(app);

app.use('/assets', express.static('assets'));
app.use('/node_modules', express.static('node_modules'));
app.use('/POS/uploads', express.static('undefined/POS/uploads'));

app.get('/', function (req, res) {
	res.sendFile(path.join(__dirname, 'indexweb.html'));
});

serverApp.listen({ port: 8080, host: '0.0.0.0' }, () =>
	console.log(`App Server Listening on PORT 8080`),
);

let kitchen = express();
let serverKitchen = http.createServer(kitchen);

kitchen.use('/assets', express.static('kitchen-screen/assets'));
kitchen.use('/node_modules', express.static('kitchen-screen/node_modules'));
kitchen.use('/kitchen-screen', express.static('kitchen-screen'));

kitchen.get('/', function (req, res) {
	res.sendFile(path.join(__dirname, '/kitchen-screen/index.html'));
});

serverKitchen.listen({ port: 8081, host: '0.0.0.0' }, () =>
	console.log(`Kitchen Server Listening on PORT 8081`),
);

let service = express();
let serverService = http.createServer(service);

kitchen.use('/assets', express.static('service-screen/assets'));
service.use('/node_modules', express.static('service-screen/node_modules'));
service.use('/service-screen', express.static('service-screen'));

service.get('/', function (req, res) {
	res.sendFile(path.join(__dirname, '/service-screen/index.html'));
});

serverService.listen({ port: 8082, host: '0.0.0.0' }, () =>
	console.log(`Service Server Listening on PORT 8082`),
);
